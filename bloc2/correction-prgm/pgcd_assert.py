## Dans cet exemple, on montre comment utiliser des assertions pour vérifier
## l'invariant que nous avons vu dans pgcd.py à chaque tour de boucle du
## calcul. Utiliser Python avec l'option -O permet d'éviter l'exécution des
## assertions.


## Afin de vérifier les assertions, il nous faut programmer des fonctions spécifiques.
## Ici, il s'agit d'une fonction qui calcule les diviseurs d'un nombre
def diviseurs(a):
    res=[]
    for i in range(1,a+1):
        if a % i == 0:
            res=res+[i]
    return res

## Ici, nous implémentons une fonction qui calcule les diviseurs communs à deux
## nombres.
def diviseursCommuns(a,b):
    return [d for d in diviseurs(a) if d in diviseurs(b)]


def pgcd(a,b):
    # Afin de vérifier l'invariant, nous devons avoir deux variables x et y qui
    # contiennent les valeurs successives de l'algorithme. Cela nous permet de
    # toujours avoir accès aux valeurs initiales des arguments.
    x=a
    y=b
    while x > 0:
        # Notre invariant est que l'ensemble des diviseurs communs des valeurs
        # contenues dans x et y est le même que celui des valeurs initiales.
        assert diviseursCommuns(a,b) == diviseursCommuns(x,y), "PGCD: invariant"
        z=y
        y=x % y
        x=z
    return x

## Sur cet exemple, on constate que pour prendre en compte les fonctions dans
## le langage, il faut enrichir la logique de Hoare notamment en introduisant
## un accès aux valeurs initiales des arguments d'une fonction. Concètement
## cela est matérialisé ici par l'ajout des variables x et y dans la fonction
## pgcd afin de vérifier des assertions.
