---
title: Les filles dans les filières du numérique
subtitle: Inspiré d'une formation PAF, Académie de Lille
langs: fr-fr
tags: paf, orientation filles
author: |
    | Patricia Everaere, Asli Grimaud, Marie Guichard, Philippe Marquet, Mohamed Nassiri, Maude Pupin
date: 13 déc 2022
header-includes:
- \usepackage{caption}
- \captionsetup[figure]{labelformat=empty}
    \definecolor{pourpre}{RGB}{114, 0, 98}
    \definecolor{bleu_gris}{RGB}{127, 160, 172}
    \definecolor{bleu_fonce}{RGB}{29, 66, 138}
    \setbeamercolor*{structure}{bg=white,fg=pourpre}
    \setbeamercolor*{block title}{bg=bleu_fonce,fg=white}
    \setbeamercolor*{item}{bg=white,fg=bleu_gris}
...

<!-- pour générer le PDF 
pandoc -t beamer --slide-level 2 -V theme:Boadilla -s infofem_DIU-EIL.md -o infofem_DIU-EIL.pdf

ou pour un design plus épuré :
pandoc -t beamer --slide-level 2 -V "aspectratio=1610" -s infofem_DIU-EIL.md -o infofem_DIU-EIL.pdf
--> 

# Genre et stéréotypes

## Genre ou sexe ? Définitions de l'OMS
 
* Le mot **sexe** se réfère aux caractéristiques biologiques et physiologiques qui différencient les hommes des femmes, comme les organes reproductifs, les chromosomes, les hormones, _etc_.

* Le mot **genre** sert à évoquer les rôles qui sont déterminés socialement, les comportements, les activités et les attributs qu’une société considère comme appropriés pour les hommes et les femmes.

    Ref : [https://www.coe.int/fr/web/gender-matters/sex-and-gender](https://www.coe.int/fr/web/gender-matters/sex-and-gender)


## Les stéréotypes et leurs dérives

* Un **stéréotype** est une image préconçue, une représentation simplifiée d’un individu ou d’un groupe humain. Il repose sur une croyance partagée relative aux attributs physiques, moraux et/ou comportementaux, censés caractériser ce ou ces individus. Le stéréotype remplit une fonction cognitive importante : face à l’abondance des informations qu’il reçoit, l’individu simplifie la réalité qui l’entoure, la catégorise et la classe.
  
* Un **préjugé** est une opinion préconçue portant sur un sujet, un objet, un individu ou un groupe d'individus. Il est forgé antérieurement à la connaissance réelle ou à l'expérimentation : il est donc construit à partir d'informations erronées et, souvent, à partir de stéréotypes. 

* La **discrimination** est le traitement inégal et défavorable appliqué à certaines personnes en raison notamment, de leur origine, de leur nom, de leur sexe, de leur apparence physique ou de leur appartenance à un mouvement philosophique, syndical ou politique.

      stéréotypes → préjugés → discriminations


## Les stéréotypes de genre

* Les **stéréotypes de genre** constituent un sérieux obstacle à la réalisation d’une véritable égalité entre les femmes et les hommes et favorisent la discrimination fondée sur le genre. Ce sont des idées préconçues qui assignent arbitrairement aux femmes et aux hommes des rôles déterminés et bornés par leur sexe.

* Exemples
    * Stéréotype : Les filles sont douces et gentilles
    * Préjugé : Comme les femmes sont douces et gentilles, elles ne peuvent pas faire de bonnes directrices
    * Discrimination : Je ne vais pas engager une femme comme directrice car elle sera trop gentille

# Entre mythes et réalité

## Le cerveau a-t'il un sexe ?
> Le cerveau des femmes et des hommes fonctionnent de la même façon,
> les stéréotypes et leurs représentations influencent nos comportements.  
 Catherine Vidal

## Le cerveau a-t'il un sexe ?

- Exercice de mémorisation proposé à des écoliers et écolières
    - Une figure complexe doit être reproduite

:::::::::::::: {.columns align=center}
::: {.column width="40%"}
![Figure Rey-Osterrieth](figure_complexe.png)
:::
::: {.column width="60%"}
- Présenté comme du **dessin**
    - Les **filles** réussissent mieux
- Présenté comme de la **géométrie**
    - Les **garçons** réussissent mieux
:::
::::::::::::::


## Les métiers ont-ils un sexe ?
> Les choix d'orientation sont influencés par les stéréotypes liés aux métiers.  
> Françoise Vouillot

## Les métiers ont-ils un sexe ?

:::::::::::::: {.columns}
::: {.column width="30%"}
![](part_bac.png)
:::
::: {.column width="70%"}
![](part_enseign_sup.png)  
:::
::::::::::::::


## Les métiers ont-ils un sexe ?

### En occident, l'informatique est considérée comme un métier d'homme
- Métier lié aux technologies
- Marché de l'emploi porteur et fortes rémunérations
- Geeks/nerds masculins très médiatisés

### En Malaisie, l'informatique est considérée comme un métier de femme
- Ne nécessite pas de condition physique particulière
- Ni dangereux, ni salissant
- Peut être fait de chez soi en gardant ses enfants ou ses parents

### Les soins infirmiers, un métier de femme ?
- Travail manuel, port de charges lourdes
- Manipulation de produit dangereux, salissants
- Horaires chargés, travail de nuit et le week-end

## Les métiers ont-ils un sexe ?

![Les stéréotypes rationalisent les faits](portrait_salarie_num.png){width=60%}

_extrait d'une infographie de Pole Emploi, Fév 2019_ 


## Les métiers ont-ils un sexe ?

![Leadership et genre](leadership_genre.png){width=70%}

_[extrait d'une infographie de Social Builder](https://socialbuilder.org/2020/05/15/genre-et-leadership-lhabit-ne-fait-pas-la-nonne/)_


# Les métiers de l'informatique et du numérique

## De la créativité et du relationnel

### Coder/programmer est une activité créative
* Différentes solutions pour un même problème
* Chaque développeur ou développeuse a son propre style
* Créer un programme est une activité intellectuelle

### Programmer nécessite de communiquer
* Un programme répond à un besoin
* Il faut comprendre les utilisateurs pour développer un bon logiciel
* Développer un logiciel se fait souvent en équipe

## L'informatique, une grande variété de métiers

* Développeuse, architecte métier, ingénieure système, cheffe de projet, _etc._
* Les métiers et l'emploi dans l'informatique et les réseaux
    * [https://www.onisep.fr/Decouvrir-les-metiers/Des-metiers-par-secteur/Informatique-et-reseaux/Les-metiers-et-l-emploi-dans-l-informatique-et-les-reseaux](https://www.onisep.fr/Decouvrir-les-metiers/Des-metiers-par-secteur/Informatique-et-reseaux/Les-metiers-et-l-emploi-dans-l-informatique-et-les-reseaux)
* Les métiers des mathématiques, de la statistique et de l’informatique
    *  [https://www.onisep.fr/Publications/Zoom/Les-metiers-des-mathematiques-de-la-statistique-et-de-l-informatique](https://www.onisep.fr/Publications/Zoom/Les-metiers-des-mathematiques-de-la-statistique-et-de-l-informatique)
* Des débouchés pour toutes et tous
    * [https://talentsdunumerique.com/le-numerique/metiers-avenir](https://talentsdunumerique.com/le-numerique/metiers-avenir)
* Des vidéos sur les métiers et l'orientation
    * [https://talentsdunumerique.com/orientation-numerique-media](https://talentsdunumerique.com/orientation-numerique-media)
* Des vidéos d'informaticiennes
    * [https://femmes-numerique.fr/toutes-les-videos-temoignages/](https://femmes-numerique.fr/toutes-les-videos-temoignages/)


# La place des femmes dans le numérique

## Le monde du travail

![](chiffres_numerique_travail.png)

## Les études supérieures

![](chiffres_numerique_sup.png)

## Le lycée

![](chiffres_numerique_lycee.png)

## Évolution, historique 

### Femmes et informatique, une longue histoire

Petit florilège historique pour re(?)découvrir des femmes qui ont fait l'informatique

- [Ada LOVELACE](https://fr.wikipedia.org/wiki/Ada_Lovelace) (1815-1852)
    - Première informaticien·ne et conceptrice de la programmation
- [Grace HOPPER](https://fr.wikipedia.org/wiki/Grace_Hopper) (1906-1992)
    - Pionnière des langages informatiques avec la création des compilateurs
- [Frances ALLEN](https://fr.wikipedia.org/wiki/Frances_Allen) (1932-2020)
    - Première femme récipiendaire du prix Turing (Nobel) pour ses contributions sur l’optimisation des compilateurs
- [Margaret HAMILTON](https://fr.wikipedia.org/wiki/Margaret_Hamilton_(scientifique)) (1936-...)
    - Cheffe du projet du logiciel de pilotage de la mission spatiale d’Apollo

## Que s'est-il passé ?

* Seconde guerre mondiale
    * ENIAC six
* Années 60
    * "Calculatrices" de la NASA
    * Film : les figures de l'ombre
* Années 70 
    * ordi = gestion information
    * Donc métiers du tertiaire traditionnellement plus féminisés que l’industrie
* Années 80
    * Arrivée des micro-ordinateur dans les foyers
    * Les premiers équipés sont les garçons et adolescents

* Les hommes s'emparent de l'informatique


## Causes : une spirale négative

* Stéréotype : informaticien = homme blanc, à lunettes = geek
* L'informatique est associé aux sciences (math) et à la technologie qui sont associées aux hommes
* L'informatique est un domaine lucratif et de pouvoir qui attire donc les hommes
* D'où la menace de stéréotype
    * Effet d'un stéréotype ou préjugé sur une personne appartenant à un groupe visé par ce préjugé : dans une situation où il s'applique, où il risquerait de se manifester, cette personne se sent jugée et éprouve des sentiments d'anxiété ou d'insécurité. Cela risque alors d'affecter ses performances. Ainsi, les femmes ne se sentent que trop souvent peu légitimes en informatique, ce qui ne les poussent pas à se diriger vers ce domaine.
* D'où la censure sociale [^Clémence Perronnet]
    * Les domaines excluent
    * Il existe un rapport social défavorable au groupe dominé

[^Clémence Perronnet]: [https://academia.hypotheses.org/tag/censure-sociale](https://academia.hypotheses.org/tag/censure-sociale)

## Causes : l'effet Matilda

L'effet Matilda est le déni ou la minimisation récurrente et systémique de la contribution des femmes scientifiques à la recherche, dont le travail est souvent attribué à leurs collègues masculins.  

Par exemple, Grace Hopper (1906-1992), travaille pendant la Seconde Guerre mondiale sur un compilateur pour le Harvard Mark I, ordinateur dont l'architecture matérielle a été définie par John von Neumann. C'est pourtant lui qui est présenté comme l'initiateur de l'un des premiers programmes exécutés par la machine.
Cela induit une invisibilisation des femmes en situation de réussite dans le domaine scientifique et contribue au fait que les femmes ne peuvent se reconnaître dans des rôles modèles.


## Conséquences

### Constat = faible proportion de femmes 
* Dans les métiers du numérique 
* Dans les formations au numérique 
* & une évolution historique défavorable

### Pourquoi faut-il plus de femmes en informatique ?
- Les équipes sont plus performantes quand elles sont mixtes
- Pour une meilleure ambiance de travail (éviter le entre-soi)
- Pour varier les points de vue
- Pour éviter les biais sexistes, de genre dans les applications et dans l'IA
    - [déclaration de Montréal](https://www.declarationmontreal-iaresponsable.com/la-declaration) pour un développement responsable de l’intelligence artificielle
    6. Principe d’équité : Le développement et l’utilisation des systèmes d'IA (SIA) doivent contribuer à la réalisation d’une société juste et équitable.
    7. Principe d’inclusion de la diversité : Le développement et l’utilisation de SIA doivent être compatibles avec le maintien de la diversité sociale et culturelle et ne doivent pas restreindre l’éventail des choix de vie et des expériences personnelles.


## Difficulté à infléchir la tendance

:::::::::::::: {.columns}
::: {.column width="55%"}
Comme peu de filles  

* Cela n'attire pas les filles
    - Les filles intègrent le « fait » que ce n'est pas pour elles

* Peut être repoussoir
    * seule fille dans une classe d'informatique...
:::
::: {.column width="45%"}
![](whatsthematter.png)
:::
::::::::::::::


## Le numérique un domaine en tension

### Perspectives d'emploi dans le numérique 

115 000 postes supplémentaires chez les ingénieurs de l’informatique d'ici à 2030  



  Ref : [https://www.strategie.gouv.fr/publications/metiers-2030](https://www.strategie.gouv.fr/publications/metiers-2030)
    repris dans [https://www.aefinfo.fr/depeche/669250](https://www.aefinfo.fr/depeche/669250)
        et dans [https://www.lesnumeriques.com/pro/le-chiffre-du-jour-115-000-postes-supplementaires-chez-les-ingenieurs-de-l-informatique-d-ici-a-2030-n178275.html](https://www.lesnumeriques.com/pro/le-chiffre-du-jour-115-000-postes-supplementaires-chez-les-ingenieurs-de-l-informatique-d-ici-a-2030-n178275.html)

## Augmenter le vivier 

* Les entreprises du numérique sont parmi les moins paritaires, mais le manque de main-d’œuvre les pousse à favoriser la mixité hommes-femmes.
* Les raisons de prendre le taureau par les cornes sont avant tout économiques. La pénurie de compétences ne permet pas de laisser la moitié des forces disponibles sur le bas-côté.
    * [https://www.lemonde.fr/societe/article/2018/11/12/le-secteur-des-nouvelles-technologies-en-quete-de-femmes_5382305_3224.html](https://www.lemonde.fr/societe/article/2018/11/12/le-secteur-des-nouvelles-technologies-en-quete-de-femmes_5382305_3224.html)

* Si le secteur recrute, cela fait déjà plusieurs années qu’il souffre d'un manque important de candidats. Nous assistons en effet à une pénurie de talents, encore plus du côté des femmes.
    * [https://www.journaldunet.com/management/emploi-cadres/1496363-le-role-capital-des-femmes-dans-l-avenir-de-la-cybersecurite/](https://www.journaldunet.com/management/emploi-cadres/1496363-le-role-capital-des-femmes-dans-l-avenir-de-la-cybersecurite/)

## Remèdes 

### Actions du [Club informatique au féminin](https://www.linkedin.com/groups/12660932/), Univ Lille
- Sensibilisation des étudiants et étudiantes aux biais de genre
- Formation des étudiants au management inclusif
- Rendez-vous entre étudiantes
- Speed-mentorat (discussion de 30 min avec des professionnels)
- Actions de médiation en informatique auprès des plus jeunes
    - [L décodent l'@venir](]https://wikis.univ-lille.fr/chticode/wiki/infogirl/accueil), stages collectifs d'observation du monde pro

### Les actions en région
- Numériqu'elles, par le CORIF [https://www.corif.fr/](https://www.corif.fr/)
- Wi-Filles, par FACE-MEL [http://www.face-mel.fr/](http://www.face-mel.fr/)
- Semaine du numérique en Hauts-de-France, 20 au 25 mars 2022
    - [https://www.semaine-numerique-hdf.fr/](https://www.semaine-numerique-hdf.fr/)
- Journée NSI (pour le numérique en général), [https://journee-nsi.fr/](https://journee-nsi.fr/)

## Les étapes pour aboutir à une meilleure parité

### Mesurer
- Prendre conscience des inégalités

### Expliquer
- Déterminer les causes des inégalités
- Comment en est-on arrivé là ?
- Pourquoi faut-il plus de femmes en informatique ?

### Agir
- Avoir un comportement respectueux envers tous et toutes
- Ne pas seulement viser l'égalité des chances, mais casser les barrières qui freinent l'insertion de toutes et tous dans les métiers de l'informatique

## Expériences d’inclusion pérenne

### Expériences réussies
- Norwegian University of Science and Technology (NTNU)
- Carnegie Mellon University (CMU)

### Trois phases d’un processus d’inclusion
1. **Intéresser**
    - L’intérêt de candidates potentielles est suscité et l’objectif d’inclusion est largement accepté à l’intérieur de l’institution
2. **Recruter**
    - Le nombre de candidates recrutées atteint un pourcentage visé, sans susciter d’oppositions internes
3. **Socialiser**
    - Les étudiantes participent autant que les étudiants à la vie de l’institution.

Ref : Femmes et métiers de l’informatique : un monde pour elles aussi
    *Chantal Morley, Isabelle Collet*, Cahiers du Genre, 2017, (62) p183-202
[https://www.cairn.info/revue-cahiers-du-genre-2017-1-page-183.htm](https://www.cairn.info/revue-cahiers-du-genre-2017-1-page-183.htm)
