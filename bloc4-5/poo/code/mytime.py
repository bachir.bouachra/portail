

class Time:
    
    BASE = 60   # variable de classe, permettrait de remplacer tous les 60 du code par Time.BASE
    
    def __init__(self, h, m, s):
        #if (h < 0 or m < 0 or m >= Time.BASE or s < 0 or s >= Time.BASE):
        if (h < 0 or m < 0 or m >= 60 or s < 0 or s >= 60):
            raise IllegalArgumentError ('illegal argument for a time : {:d},{:d},{:d}'.format(h,m,s))
    
        self.__hours = h
        self.__minutes = m
        self.__seconds = s
        
    
    def get_hours(self):
        return self.__hours
    
    def get_minutes(self):
        return self.__minutes
    
    def get_seconds(self):
        return self.__seconds

            
    def to_seconds(self):
        '''
        >>> Time(1,1,1).to_seconds() == 3661
        True
        >>> Time(0,1,1).to_seconds() == 61
        True
        '''
        return (self.get_hours() * 60 + self.get_minutes()) * 60 + self.get_seconds()
        # return (self.__hours * 60 + self.__minutes) * 60 + self.__seconds
    
    def compare(self, other):
        return self.to_seconds() - other.to_seconds()
    
    @staticmethod
    def from_seconds(nb_seconds):
        '''
        >>> Time.from_seconds(3661) == Time(1,1,1)
        True
        >>> Time.from_seconds(61) == Time(0,1,1)
        True
        '''
        hours = nb_seconds // 3600
        minutes = ( nb_seconds % 3600 ) // 60
        seconds = nb_seconds % 60 #(nb_seconds % 3600) % 60
        return Time(hours, minutes, seconds)


        
    def __eq__(self, other):
        return self.compare(other) == 0
    
    def __lt__(self, other):
        return self.compare(other) < 0
    
    def __str__(self):
        '''
        >>> Time(1,1,1) + Time(2,2,2) == Time(3,3,3)
        True
        >>> Time(1,1,2) + Time(2,58,59) == Time(4,0,1)
        True
        '''
        return "{}h{:2}mn{:2}s".format(self.get_hours(), self.get_minutes(), self.get_seconds())
        #return "{}h{:2}mn{:2}s".format(self.__hours, self.__minutes, self.__seconds)

    def add(self,other):
        return Time.from_seconds(self.to_seconds() + other.to_seconds())
    
    def __add__(self,other):
        return self.add(other)

    '''
    def __repr__(self):
        return 'time : '+ str(self)
    '''
    

class IllegalArgumentError (Exception):
    """
    """
    def __init__ (self, msg):
        self.message = msg
    
    
    
if __name__ == '__main__':
    import doctest
    doctest.testmod()
    t1 = Time(1,1,1)
    t2 = Time(2,2,2)
    t3 = Time(1,1,1)
    
    