from PIL import Image
import numpy


tab = [[5], [8, 10], [11, 3, 4], [6, 10, 7, 12]]


def solution_naive(t, ligne, r):
    """
    :param t: (list) array of numbers
    :param l: (int) line
    :param r: (int) row
    :return: (int) best cumulative sum

    :Example:
    >>> solution_naive([[1], [2, 3]], 0, 0)
    4
    """
    if ligne == len(t)-1:
        return t[ligne][r]
    else:
        return t[ligne][r]+max(solution_naive(t, ligne+1, r),
                               solution_naive(t, ligne+1, r+1))


def solution_dynamique(tab):
    """
    :param t: (list) array of numbers
    :param l: (int) line
    :param r: (int) row
    :return: (int) best cumulative sum

    :Example:
    >>> solution_dynamique([[1], [2, 3]])
    4
    """
    n = len(tab)
    gain = [[0 for r in range(ligne+1)]
            for ligne in range(n)]
    for col in range(n):
        gain[n-1][col] = tab[n-1][col]
    for ligne in range(n-2, -1, -1):
        for col in range(0, ligne+1):
            gain[ligne][col] = tab[ligne][col] + max(gain[ligne+1][col],
                                                     gain[ligne+1][col+1])
    return gain[0][0]


def pixel_energy(p1, p2):
    """
    :param p1: (tuple) first pixel
    :param p2: (tuple) second pixel
    :return: (int) sum of square of difference by composante
    :Example:
    >>> pixel_energy((1, 2, 3), (2, 3, 4))
    3
    """
    r1, g1, b1 = p1
    r2, g2, b2 = p2
    return (r1-r2)**2 + (g1-g2)**2 + (b1-b2)**2


def neighbors(coord, dim):
    """
    :param coord: (tuple) les coordonnées d'un pixel
    :param dim: (tuple) dimensions de l'image
    :return: (tuple) neighbors of coord
    :examples:
    >>> neighbors((0,0),(10,10))
    ((0, 0), (1, 0), (0, 0), (0, 1))
    >>> neighbors((10,5), (10,10))
    ((9, 5), (10, 5), (10, 4), (10, 6))
    >>> neighbors((4, 4), (10,10))
    ((3, 4), (5, 4), (4, 3), (4, 5))
    """
    x, y = coord
    w, h = dim
    if x > 0:
        p1 = x-1, y
    else:
        p1 = x, y
    if x < w-1:
        p2 = x+1, y
    else:
        p2 = x, y
    if y > 0:
        p3 = x, y-1
    else:
        p3 = x, y
    if y < h-1:
        p4 = x, y+1
    else:
        p4 = x, y
    return (p1, p2, p3, p4)


def image_energy(im):
    """
    :param im: (Image) an RGB image
    :return: (np.array) array of image's energy
    :CU: None
    """
    w, h = im.size
    res = numpy.zeros((h, w), dtype=int)
    for y in range(h):
        for x in range(w):
            c1, c2, c3, c4 = neighbors((x, y), (w, h))
            p1 = im.getpixel(c1)
            p2 = im.getpixel(c2)
            p3 = im.getpixel(c3)
            p4 = im.getpixel(c4)
            res[y, x] = pixel_energy(p1, p2) + pixel_energy(p3, p4)
    return res


def seams_energy(en):
    """
    :param en: (np.array) the array of pixel energy
    :return: (np.array) the array of energy of seams of minimum energy
    :CU: None
    """
    h, w = en.shape
    res = numpy.zeros((h, w), dtype=int)
    for x in range(w):
        res[0, x] = en[0, x]
    for y in range(1, h):
        for x in range(w):
            pdessus = [res[y-1, x]]
            if x > 0:
                pdessus.append(res[y-1, x-1])
            if x < w-1:
                pdessus.append(res[y-1, x+1])
            res[y, x] = en[y, x] + min(pdessus)
    return res


def minimal_seam(min_energy):
    """
    :param min_energy: (np.array) array of energy of minimal energy seams
    :return: (list) list of coordinates of the seam
    """
    h, w = min_energy.shape
    m = min(range(w), key=lambda x: min_energy[h-1, x])
    x = m
    y = h-1
    res = []
    res.append((x, h-1))
    while y > 0:
        au_dessus = [(x, y-1)]
        if x > 0:
            au_dessus.append((x-1, y-1))
        if x < w-1:
            au_dessus.append((x+1, y-1))
        x, y = min(au_dessus, key=lambda c: min_energy[c[1], c[0]])
        res.append((x, y))
    return res


def remove_from_width(im, n):
    """
    :param im: (Image) an image
    :param n: (int) number of pixels to remove from width
    :return: (Image) the resized image
    """
    w, h = im.size
    assert w > n, "image trop étroite"
    for i in range(1, n+1):
        en = image_energy(im)
        seams_en = seams_energy(en)
        next_im = Image.new('RGB', (w-i, h),
                            (255, 255, 255))
        seam = minimal_seam(seams_en)
        for y in range(h):
            x_seam, _ = seam.pop()
            for x in range(x_seam):
                next_im.putpixel((x, y), im.getpixel((x, y)))
            for x in range(x_seam+1, w-i):
                next_im.putpixel((x-1, y), im.getpixel((x, y)))
        im = next_im
    return im


def main():
    surf = Image.open("surfer.jpg").convert('RGB')
    print(f"dimension de l'image {surf.size}")
    print(surf.getpixel((100, 200)))
    en = image_energy(surf)
    h, w = en.shape
    energy_map = Image.new("L", (w, h), 255)
    for y in range(h):
        for x in range(w):
            color = int(en[y, x] // 16)
            energy_map.putpixel((x, y), color)
    energy_map.show()
    s_en = seams_energy(en)
    seam = list(minimal_seam(s_en))
    for p in seam:
        surf.putpixel(p, (255, 0, 0))
    surf.show()
    # mod_surf = remove_from_width(surf, 100)
    # mod_surf.show()


if __name__ == "__main__":
    main()
