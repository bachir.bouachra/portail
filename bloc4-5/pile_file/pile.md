Utilisation de piles et files
=============================

L'objectif général du TP est d'utiliser des piles et des files pour vérifier qu'un texte est bien formé.  
On débutera avec la vérification du bon parenthésage d'expressions. 
On poursuivra avec la vérification de fichiers HTML. 

Les réalisations attendues sont identifiées par des **:white_check_mark: à faire**. 

## 1. Vérificateur d'expressions bien parenthésées ##

### Expression bien parenthésée ###

Une expression est une suite de caractères.  
Parmi ces caractères, nous considérons particulièrement les parenthèses de différents types : `(` et `)`, `[` et `]` , `{` et `}`, etc. 
Nous ignorons les autres caractères. 

Une expression est bien parenthésée si :

- à chaque parenthèse ouvrante correspond une parenthèse fermante,
- à tout moment, on ne peut fermer une parenthèse que si l'expression située entre les deux parenthèses se correspondant est bien parenthésée.

Par exemple, les expressions suivantes sont bien parenthésées :

-   `((()))` 
-   `()()()`
-   `([{}]())`

Les expressions suivantes ne sont pas bien parenthésées :

-   `(()))`
-   `(()`
-   `([)]`

### Expression = file de caractères ###

Les *files* sont des structures séquentielles d'éléments agissant comme une file d'attente. 
On peut ajouter et retirer  des éléments de la structure, mais les premiers éléments ajoutés seront toujours les premiers à sortir.
La file est une structure FIFO — premier arrivé, premier sorti, _First In, First Out_. 
On parle de _queue_ en anglais.

La structure de file dispose des primitives suivantes :

- création d'une file vide,
- enfilement, _enqueue_, ajoute un élément à la file, 
- défilement, _dequeue_, enlève l'élément le plus anciennement ajouté à la file et le renvoie, 
- test de vacuité d'une file. 

Pour les besoins de ce TP, nous considérons qu'une expression se réduit à un flux de parenthèses (lu du premier caractère au dernier caractère). 
Les autres caractères sont ignorés.   

On utilisera une file pour mémoriser ce flux. 

Le module `queue` de la librairie standard Python propose une telle structure. 
On, utilisera la classe `SimpleQueue` et les trois méthodes `put()`, `get()`, et `empty()` pour respectivement enfiler une valeur, défiler une valeur, et tester la vacuité de la structure. 

**:white_check_mark: à faire** Essayer :

```python 
from queue import SimpleQueue

# création d'une file vide
q = SimpleQueue()

# elle est bien vide 
q.empty()

# on enfile une valeur
q.put('a')

# elle n'est plus vide
q.empty()

# on enfile une seconde valeur
q.put('z')

# on défile une valeur 
v = q.get()

# c'est bien la première qui avait été enfilée
v == 'a'

# on défile une seconde valeur
v = q.get()

# la file est maintenant vide
q.empty()
```

**:white_check_mark: à faire** Proposer une fonction `parse_expr()` qui accepte une expression sous forme de chaîne de caractères en paramètre et renvoie une file, valeur de type `SimpleQueue`, flux des caractères parenthèses de l'expression.

On aura par exemple

```python
>>> q = parse_expr('( a + {b*c]')
>>> q.get() == '(' and q.get() == '{' and q.get() == ']' and q.empty()
True
```

### Algorithme du bon parenthésage ###

Pour vérifier qu'une expression est correctement parenthésée, une méthode est de parcourir le flux de caractères correspondant à l'expression, et d'utiliser une pile :

- lorsque l'on rencontre une parenthèse ouvrante on l'empile,
- lorsque l'on rencontre une parenthèse fermante, par exemple `]` :
    - si la pile est vide, alors l'expression est mal parenthésée (trop de fermantes), 
    - sinon, on  dépile l'ouvrante située au sommet de la pile et on vérifie que les deux parenthèses correspondent (`(` et `)`, `[` et `]`, etc.).

Lorsque le flux de caractères a été parcouru, la pile doit être vide (pas d'ouvrante non fermée).

### Pile de parenthèses ###

Les *piles* sont des structures séquentielles d'éléments admettant les *opérations primitives* suivantes :

- création d'une pile vide,
- empilement, _push_, d'un élément sur une pile,
- dépilement, _pop_, du sommet d'une pile,
- test de vacuité d'une pile.

Le principe d'une pile est que l'on ne peut accéder à un élément $`e`$ de la pile qu'en ayant enlevé d'abord tous les éléments empilés après $`e`$.  
L'élément renvoyé par la primitive _pop_ est le dernier élément empilé. 
On dit que la pile est une structure LIFO – dernier arrivé, premier sorti, _Last In, First Out_ —. 

Les listes Python munies des seules primitives suivantes peuvent être utilisées comme piles : 

* pile vide, utilisée pour la création et le test de vacuité : `[]` 
* empiler une valeur `v` sur une pile `p` : `p.append(v)`
* dépiler le sommet d'une pile `p` : `v = p.pop()`

Par exemple : 

```python
>>> p = []
>>> p.append('a')
>>> p.append('z')
>>> p.pop()
'z'
>>> p.pop()
'a'
>>> p == []
True

```

Pour s'assurer de n'utiliser aucune autre primitive, on définit une classe `Stack` dont l'implémentation repose sur ces listes. 

**:white_check_mark: à faire** Compléter le squelette suivant qui se trouve aussi dans [`stack_squel.py`](./src/stack_squel.py) : 

```python
class StackEmptyError(Exception):
    """
    exception pour pile vide
    """
    def __init__(self, msg):
        self.message = msg

class Stack:
    """
    une classe pour manipuler les piles
    """

    def __init__(self):
        """
        constructeur de pile
        """
        pass

    def is_empty(self):
        """
        :return: (bool) True si la pile est vide, False sinon
        :CU: None
        :Exemples:

        >>> p = Stack()
        >>> p.is_empty()
        True
        >>> p.push(1)
        >>> p.is_empty()
        False
        """
        pass

    def push(self, el):
        """
        :param el: (any) un élément
        :return: None
        :Side-Effet: ajoute un élément au sommet de la pile
        :CU: None

        >>> p = Stack()
        >>> p.push(1)
        >>> p.pop() == 1
        True
        """
        pass

    def pop(self):
        """
        :return: (any) l'élément au sommet de la pile
        :CU: la pile ne doit pas être vide
        :raise: StackEmptyError
        :Side-Effect: la pile est modifiée
        :Exemples:

        >>> p = Stack()
        >>> p.push(1)
        >>> p.pop() == 1
        True
        >>> p.is_empty()
        True
        """
        pass

    def top(self):
        """
        :return: (any) l'élément au sommet de la pile
        :CU: la pile ne doit pas être vide
        :raise: StackEmptyError
        :Exemples:

        >>> p = Stack()
        >>> p.push(1)
        >>> p.top() == 1
        True
        >>> p.is_empty()
        False
        """
        pass

if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)
```

### Piles et files avec les listes Python ###

Nous avons proposé d'utiliser les listes Python pour mettre en œuvre les piles, mais pas pour mettre en œuvre les files.  
Cela est motivé par le coût des différentes opérations sur les listes Python. 

Nous pourrions utiliser une liste Python pour représenter une file :

* une nouvelle valeur à enfiler serait ajoutée en fin de liste avec la méthode `append()`, 
* la plus ancienne valeur enfilée serait donc en début de liste.  
  Il serait nécessaire de la supprimer, par exemple avec l'instruction `del`.

Or les opérations de suppression (mais aussi d'insertion) en début de liste ne sont pas performante. 
Grossièrement, il est nécessaire de décaler les autres éléments de la liste d'une position. 

En raison de ce choix d'implantation des listes fait par Python, il est préférable d'utiliser le module `queue` (ou autre, tel `dequeue`), comme nous l'avons fait.  
Il est aussi possible d'implanter soit même un module de gestion de files.
C'est ce que nous proposerons plus loin dans ce document ! 

On consultera éventuellement la documentation Python de `docs.python.org` qui mentionne ce sujet : 

* [_Utilisation des listes comme des piles_](https://docs.python.org/fr/3/tutorial/datastructures.html#using-lists-as-stacks),
* [_Utilisation des listes comme des files_](https://docs.python.org/fr/3/tutorial/datastructures.html#using-lists-as-queues).

### Bon parenthésage ###

Maintenant que nous disposons de classes pour manipuler files et piles, nous pouvons mettre en œuvre l'algorithme de bon parenthésage. 

**:white_check_mark: à faire** Réaliser un prédicat `bien_parenthesee()` qui accepte en paramètre une `SimpleQueue` de parenthèses et indique s'il est bien parenthésée ou non. 

## 2. Validateur HTML ##

### Fichier au format HTML bien formé ###

Le HTML est un format de fichier utilisé par les navigateurs web.
Les fichiers au format HTML (et plus généralement au format XML) sont des fichiers texte dans lesquels on trouve des balises :

- ouvrantes de la forme `<nom attributs>`,
- fermantes de la forme `</nom>`,
- auto-fermantes de la forme `<nom attributs/>`.

où `nom` désigne le nom de la balise et `attributs` une liste éventuellement vide de couples `clé=valeur`.

Dans la pratique, `nom` est par exemple `div`, `p`, `html`, `body`, `head`, etc.
Pour la syntaxe des tags HTML, voir [cette page de `w3.org`](https://www.w3.org/community/webed/wiki/HTML/Training/Tag_syntax).
Les balises auto-fermantes sont : `area`, `br`, `hr` , `img` , `input` , `link` , `meta` et `param`. 

Dans cette activité, on considère qu'un document HTML est bien formé si :

- à chaque balise ouvrante correspond une balise fermante,
- on ne peut fermer une balise que si toutes les balises situées entre les deux balises ouvrantes et fermantes correspondantes sont fermées. 

Un document HTML peut être vu comme une expression parenthésée :

- les tags ouvrants `<tag>` sont les parenthèses ouvrantes;
- les tags fermants `</tag>` sont les parenthèses fermantes.

Par exemple, les documents [ex1.html](./data/ex1.html) et [ex4.html](./data/ex4.html) sont bien formés, alors que les documents [ex2.html](./data/ex2.html) et
[ex3.html](./data/ex3.html) sont mal formés. 

On veut écrire un prédicat renvoyant `True` si un texte est un document HTML bien formé et `False` dans le cas contraire.

Dans ce cadre, on peut donc ne pas tenir compte des balises auto-fermantes (et on ne demande pas de vérifier ici si les balises sont des balises HTML existantes, ni si on respecte les attributs des balises).

### Premier parser HTML ###

La première étape est d'écrire un parser de fichier HTML, permettant de parcourir séquentiellement les balises.

L'écriture d'un tel parser est une tâche difficile, car un caractère `<` peut être rencontré dans différentes situations :

-   situation 1 : définition du type du document : `<!DOCTYPE ...>`
-   situation 2 : signe inférieur dans le texte : `i < len(l)`
-   situation 3 : commentaires HTML `<!--` `-->`
-   situation 4 : signe inférieur dans des attributs : `<script data-user=">myfunc();">` 

Pour simplifier les choses nous commencerons par traiter uniquement des fichiers HTML avec la situation 1, puis nous autoriserons dans une deuxième version les 
documents HTML contenant les situations 2 à 4.

On rappelle que les balises auto-fermantes peuvent être ignorées. 

Par souci de simplification on pourra considérer que les attributs d'une
balise ne peuvent être séparés que par des espaces.

**:white_check_mark: à faire** Écrire une première version du parser :  la fonction `parse(document)` prend en paramètre un document sous forme d'une chaîne de caractères et renvoie une file contenant les tags.

On utilisera à profit les méthode `index` ou `find` des chaînes de caractères. 
La méthode `split` peut également s'avérer utile.
Consulter par exemple la documentation sur `docs.python.org` : 

* [`index`](https://docs.python.org/fr/3/library/stdtypes.html#str.index) 
* [`find`](https://docs.python.org/fr/3/library/stdtypes.html#str.find) 
* [`split`](https://docs.python.org/fr/3/library/stdtypes.html#str.split)

### Premier vérificateur HTML ###

Dès que l'on peut récupérer les tags séquentiellement, nous pouvons écrire un vérificateur HTML (checker). 

On fait bien entendu l'analogie entre les documents HTML et les expressions correctement parenthésées. 

On utilisera donc le même algorithme de vérification du bon parenthésage. 
Sur la pile, on placera des tags ouvrants.

**:white_check_mark: à faire** Réaliser une fonction 

```python
def html_checker(s):
    """
    :param s: (str) un document html
    :return: (bool) True si `s` est bien formé, False sinon
    :CU: Aucune
    :Exemple:

    >>> html_checker("<!DOCTYPE html><html lang='fr'><div><p></p></div></html>")
    True
    >>> html_checker("<!DOCTYPE html><html lang='fr'><div></p></div><p></html>")
    False
    >>> html_checker("<!DOCTYPE html><html lang='fr'><div><p></p></div>")
    False
    """
```

## 3. Approfondissements, améliorations ##

Des approfondissements et améliorations peuvent maintenant être proposées. 

### Nouvelle implémentation des piles ###

Afin de nous affranchir de l'implémentation des listes Python, nous pouvons réaliser un module de manipulation de piles qui propose les primitives empiler, dépiler en _temps constant_, indépendant de la taille de la pile. 

**:white_check_mark: à faire** S'inspirer du [travail fait sur les
listes](../listes/readme.md) pour proposer une définition (récursive)
d'une pile.  
En déduire une représentation d'une pile permettant d'ajouter et de supprimer un élément en sommet de pile en temps constant. 

**:white_check_mark: à faire** Proposer une nouvelle réalisation du module `stack` à partir du squelette [stack_squel.py](./src/stack_squel.py).

### Amélioration du parser avec des expressions régulières ###

L'objectif est ici d'écrire une nouvelle version du parser permettant de prendre en compte l'ensemble des situations particulières listées plus haut. 

Pour cela, nous pouvons utiliser une expression régulière. 
Une expression régulière est chaîne de caractères spécifiant un format permettant de :

-   dire si une chaîne correspond au format ;
-   capturer certaines parties de ce format.

Un cours sur les expressions régulières est hors du programme du DIU, mais nous allons en fournir une pour récupérer les tags d'un document HTML.

Le code suivant contient une expression régulière présentée sur plusieurs lignes pour plus de lisibilité. 
Elle permet de récupérer les tags en tenant compte des guillemets.

```python
import re

HTML_REGEX = re.compile(
    r"""<                        # commence par un <
(/?\w+)                          # capture le nom du tag (éventuellement avec un slash
(                                # groupe des attributs
 (
  \s+                            # autant d'espace que l'on veut
  \w+                            # suivi d'un mot (nom de l'attribut)
  (
   \s*=\s*                       # un signe égal, entouré d'espaces
   (?:".*?"|'.*?'|[\^'">\s]+)    # valeur de l'attribut : capture de tout ce qui est entre guillemets
  )?                             # valeur optionnelle
 )+                              # autant de couple nom=valeur que l'on veut
 \s*                             # suivi d'éventuelles espaces
 |\s*                            # ou bien pas d'attributs : que des espaces
)                                # fin des attributs
>                                # finit par un >
""", re.VERBOSE)
html = """
<!DOCTYPE html>
<!-- un commentaire -->
<html lang="fr-FR">
  <body>
    <h1> Un titre </h1>
    <p class='code'> 
         un paragraphe 
         while i < len(l):
    </p>
    <br/>
  </body>
</html>
"""
for tag_element in HTML_REGEX.findall(html):
    print(tag_element[0])
```

    html
    body
    h1
    /h1
    p
    /p
    /body
    /html

**:white_check_mark: à faire** En utilisant cette expression régulière, ou sa forme condensée (ci-dessous), écrire une deuxième version du parser.

Forme condensée de l'expression régulière :

```python
HTML_REGEX = re.compile(
    r"""<(/?\w+)((\s+\w+(\s*=\s*(?:".*?"|'.*?'|[\^'">\s]+))?)+\s*|\s*)>""")
```

### Bibliothèque pour parser du HTML ###

Utiliser une expression régulière a toujours ses limites pour parser un fichier HTML. 
Des bibliothèques existent pour itérer sur les tags d'un tel document.

Parmi elles, on peut utiliser la classe `HTMLParser`. 
Voici le code d'une classe héritant de `HTMLParser` et qui construit la liste des tags :

```python
from html.parser import HTMLParser

class MyHTMLParser(HTMLParser):
    """
    a class that parse a document and allow tag access
    :exemples:

    >>> parser = MyHTMLParser("<!DOCTYPE hml><html lang="fr"></html>")
    >>> parser.has_tag()
    True
    >>> parser.next_tag() 
    '<html>'
    >>> parser.next_tag()
    '</html>'
    >>> parser.has_tag()
    False
    """    
    def __init__(self, data):
        """
        constructor for MyHTMLParse
        :param data: (str) html document
        :UC: None
        """
        super().__init__()
        self.__tags = []
        self.__tag_index = 0
        HTMLParser.feed(self, data)

    def handle_starttag(self, tag, attrs):
        """
        handle an opening tag
        :param tag: (str) the opening tag
        :param attrs: (list) attributes
        """
        self.__tags.append('<{:s}>'.format(tag))

    def handle_endtag(self, tag):
        """
        handle an ending tag
        :param tag: (str) the ending tag
        """
        self.__tags.append('</{:s}>'.format(tag))

    def has_tag(self):
        """
        :return: (bool) True if document contains another tag, False otherwise
        """
        return self.__tag_index < len(self.__tags)

    def next_tag(self):
        """
        :return: (str) the next tag in document
        """
        res = self.__tags[self.__tag_index]
        self.__tag_index += 1
        return res

if __name__ == '__main__':
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)
```

**:white_check_mark: à faire** Proposer une nouvelle version du vérificateur de documents HTML sur la base de ce parser. 

<!--eof -->

