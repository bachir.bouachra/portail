# Exemple de script mal sécurisé (donc exemple A NE PAS SUIVRE)
# qui permet un injection SQL malveillante
# (cc) Creative Commons bruno.bogaert@univ-lille.fr

import sqlite3
fichierDonnees='injection.db'
conn = sqlite3.connect(fichierDonnees)

cur = conn.cursor()
id = input('indiquez un identifiant : ')
while id !="" :
    # /!\ NE PAS procéder ainsi :
    # ne pas utiliser format() , ni les f-strings non plus, pour insérer id dans la requête SQL
    # voir bonne solution dans injection_no.py
    cur.execute(
      "select nom from users where ident='%s' " %(id)
    )
    res = cur.fetchone()
    if res is None :
        print(f"aucun utilisateur d'identifiant {id}")
    else :
        print(f"L'utilisateur d'identifant {id} s'appelle {res[0]}")
    id = input('indiquez un identifiant : ')


conn.close()


